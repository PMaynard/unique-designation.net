---
title: Unique Designation
---

This site is dedicated to [Information-Centric Networking (ICN)](https://en.wikipedia.org/wiki/Information-centric_networking).

# Recent Activity 

A list of activities relating in some way to the information centric paradigm. 

- __2020-06-12__ Authenticating Shared Web Caches [`#interesting`] [Link](https://jamey.thesharps.us/2020/06/13/authenticating-shared-web-caches/)
- __2020-03-10__ Data Oriented Architecture (DOA) [`#interesting`] [Link](https://blog.eyas.sh/2020/03/data-oriented-architecture/)  

# Standards

The world of ICN is quickly changing and is actively being developed by many different people with differing interests. This section will try to maintain a selected subset list of useful standards for newbies and implementers alike. 

- [ICN Ping Protocol Specification](https://datatracker.ietf.org/doc/draft-mastorakis-icnrg-icnping/)
- [ICN Traceroute Protocol](https://datatracker.ietf.org/doc/draft-mastorakis-icnrg-icntraceroute/)
- [Internet Protocol Tunneling Over Content Centric Mobile Networks](https://datatracker.ietf.org/doc/draft-irtf-icnrg-ipoc/)
- [Design Guidelines for Name Resolution Service in ICN](https://datatracker.ietf.org/doc/draft-irtf-icnrg-nrs-requirements/)
- [Information-Centric Networking: Baseline Scenarios](https://datatracker.ietf.org/doc/rfc7476/)

# Getting Started with...

- __Developing ICN applications__: See [NDN](https://named-data.net/codebase/platform/) for reference implementations and libraries. They also have an international testbed. 
- __Running my very own ICN testbed__: [Mini-NDN](https://github.com/named-data/mini-ndn): A Mininet based NDN emulator
- __Understanding ICN__: As with many things [Wikipedia's ICN page](https://en.wikipedia.org/wiki/Information-centric_networking) has the answers. However, in this case (early 2020) it needs updating.
- __Proposing a new standard__: The IETF has a research group called [Information-Centric netwrking Research Group (ICNRG)](https://irtf.org/icnrg), which is the current driving force for ICN. It was chartered on 2012 April 17th.
- __Finding the latest standards__: The latest standards can be found at [ICNRG](https://datatracker.ietf.org/rg/icnrg/documents/)

It is strongly recommended that you sign up with the NDN and ICNRG mailing lists to keep updated with the ongoing developments. 

# Guides 

Due to the fluid state of ICN there are a lack of practical guides. Feel free to write some. 

My current todo list:

- An Introduction to ICN  
- My First Interest: Speaking ICN  
- A guide to ICN Ping 
- A guide to ICN Tracepath 

# Academic Publications

- [IEEE HotICN](http://hoticn.com/) Since 2018
- [ACM ICN](https://dblp.uni-trier.de/db/conf/acmicn/) Since 2011

# History of this site 

This site was created on the 10th of March 2020, by [Peter Maynard](https://petermaynard.co.uk). [This has made a lot of people very angry and been widely regarded as a bad move](https://www.goodreads.com/author/show/4.Douglas_Adams). The site's sole objective is to raise awareness of ICN and to provide newcomers with information to get started. If you spot a mistake, or wish to add something you may do so at <https://gitlab.com/PMaynard/unique-designation.net/>. Which is where the source code for this site is located. All content found on this site, and in the source repository is under CC BY-SA 4.0.

- 2020 March 10: Initial Commit. (Pete)
	- Sections: Recent Activity, Standards, Getting started, Guides, Academic Publications, and History of this site. 

# Contact 

Any questions, comments, or concerns send them to Pete (either email or matrix) `<pete>@port22.co.uk`. Want to work on a research project? Me too! Get in touch.
